FROM node:10.15-alpine
WORKDIR /home/app
COPY package.json .
RUN npm install
COPY . .
EXPOSE 3030
ENV NODE_ENV production
ENV PORT 3030
CMD ["npm", "start"]
