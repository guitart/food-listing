// const { populate } = require('feathers-hooks-common');

module.exports = {
  before: {
    all: [
      function (context) {
        context.params.query.$populate = [{ path: 'user', select: 'name' }, { path: 'food', select: 'day' }];
      }
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
