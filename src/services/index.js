const food = require('./food/food.service.js');
const user = require('./user/user.service.js');
const listing = require('./listing/listing.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(food);
  app.configure(user);
  app.configure(listing);
};
