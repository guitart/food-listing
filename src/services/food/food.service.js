// Initializes the `food` service on path `/food`
const createService = require('feathers-mongoose');
const createModel = require('../../models/food.model');
const hooks = require('./food.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/food', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('food');

  service.hooks(hooks);
};
