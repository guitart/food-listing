// listing-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const listing = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'user', required: true },
    food: { type: Schema.Types.ObjectId, ref: 'food', required: true },
    type: { type: String, required: true, defualt: ''}
  }, {
    timestamps: true
  });

  return mongooseClient.model('listing', listing);
};
