// food-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const food = new Schema({
    name: { type: String },
    day: { type: Date, required: true },
    image: { type: String, required: true},
    sideDish: [{type: String}]
  }, {
    timestamps: true
  });

  return mongooseClient.model('food', food);
};
