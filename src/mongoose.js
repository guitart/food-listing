const mongoose = require('mongoose');

module.exports = function (app) {
  const connect = function() {
    mongoose.connect(
      app.get('mongodb'),
      { useCreateIndex: true, useNewUrlParser: true, reconnectTries: Number.MAX_VALUE, reconnectInterval: 500, bufferMaxEntries: 0 },
      connect
    );
  };
  connect();
  mongoose.Promise = global.Promise;

  app.set('mongooseClient', mongoose);
};
